cmake_minimum_required(VERSION 3.1)
project(nlohmann-json)

set (CMAKE_CXX_STANDARD 17)

if(MSVC)
	# Force to always compile with W0
	if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
		string(REGEX REPLACE "/W[0-4]" "/W0" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
	else()
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W0")
	endif()
	add_compile_options(/FC)
else()
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -fPIC")
	#-fvisibility=hidden
endif()

include_directories("include")

file(GLOB_RECURSE PROJECT_HEADERS include/*.hpp)
file(GLOB PROJECT_CONFIGS CMakeLists.txt README.md)


#-----------------------------------------------------------------------------
# Helpers
#-----------------------------------------------------------------------------
function(assign_source_group)
  foreach(_source IN ITEMS ${ARGN})
	  if (IS_ABSOLUTE "${_source}")
		  file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${_source}")
	  else()
		  set(_source_rel "${_source}")
	  endif()
	  get_filename_component(_source_path "${_source_rel}" PATH)
	  string(REPLACE "/" "\\" _source_path_msvc "${_source_path}")
	  source_group("${_source_path_msvc}" FILES "${_source}")
  endforeach()
endfunction(assign_source_group)


#source_group("Headers"  FILES ${PROJECT_HEADERS})
assign_source_group(${PROJECT_HEADERS})
